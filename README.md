# Sample Twine Game

This is a sample of what I'd setup for a Twine game with collaborative development.

## Directories

These directories are just an idea as to what we would do, not definitive.

* archive
  * This is where an archive which can be imported into Twine goes.
* utils 
  * These are the tools to handle various aspects of the development process:
    * archive.sh
      * Builds an Archive that can be easily imported into Twine
    * build.sh
      * Builds a release build
    * tweego-file-splitter.py
      * Splits the Twee source into individual files.
      * This was from [https://github.com/Zachac/Tweego-File-Splitter] with a modification to remove the position from the filenames.
* published
  * This is the final published story.
* twee-source
  * This is the Twee version of the project
  * story.twee
    * This is the whole story
  * split-passages
    * This is the passages split into individual files

## Editing

This story here was created in Twine, and then exported as a twee source.

It can be edited in twine by compiling to an archive, and importing that archive.

If you want to edit just these files.  It is best to edit just the split passages.

## Notes

This is just the start of being able to work with this type of setup.

