#!/bin/bash
# This is how to build for release


VERSION=`cd $BUILD_DIR && ls -1dv [0123456789]* | tail -1`

echo ${VERSION}

echo ${BUILD_DIR}/${VERSION}/${INDEXSOURCE}

tweego ${TWEESOURCE}/*.twee > "${BUILD_DIR}/${VERSION}/${INDEXSOURCE}"
