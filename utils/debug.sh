#!/bin/bash
source=$1

cp ${source} ${source}-orig
cat ${source} | sed "s/options=\"\"/options=\"debug\"/" > ${source}-debug
cp ${source}-debug ${source}
