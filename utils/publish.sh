#!/bin/bash

## Set this in "start work" and comment these versions out
#Update to represent your project
#PROJECT="username/project-name"
#Update to represent your name for your project file
#INDEXSOURCE="sourcefile.html"

cd ${BUILD_DIR}
VERSION=`ls -1dv [0123456789]* | tail -1`

echo ${VERSION}

cp -r $VERSION/* publish
cp "$VERSION/$INDEXSOURCE" publish/index.html

~/.config/itch/apps/butler\ 2/butler status $PROJECT:html-beta
~/.config/itch/apps/butler\ 2/butler status $PROJECT:linux-osx-windows-html-beta

~/.config/itch/apps/butler\ 2/butler push ./publish $PROJECT:html-beta --userversion ${VERSION}
~/.config/itch/apps/butler\ 2/butler push ./publish $PROJECT:linux-osx-windows-html-beta --userversion ${VERSION}

sleep 10 

~/.config/itch/apps/butler\ 2/butler status $PROJECT:html-beta
~/.config/itch/apps/butler\ 2/butler status $PROJECT:linux-osx-windows-html-beta
