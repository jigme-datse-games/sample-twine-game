#!/bin/bash
# This sets variables for working with the project more easily
#

#Update to represent your project
export PROJECT="username/project-name"
#Update to represent your name for your project file
export INDEXSOURCE="sourcefile.html"

THISPATH="$(dirname -- "${BASH_SOURCE[0]}")"
FULLPATH="$(cd -- "$THISPATH" && pwd)"

export BUILD_DIR="$(cd -- "$FULLPATH" && cd ../published && pwd)"
export TWEESOURCE="$(cd -- "$FULLPATH" && cd ../twee-source/split-passages && pwd)"

export PATH=$PATH:$FULLPATH
